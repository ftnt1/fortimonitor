#!/usr/bin/env python3

"""fortimonitor.py: Simple monitoring script for FortiWhatever and not only"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"
__requires__ = ['paramiko']

import paramiko
import time
import sys

# configuration
server_IP = "192.168.117.101"
username = "admin"
password = ""
port = 22
command = "exe time\nexe date\nget sys mgmt-csum all\nexit"
logfile = "example.log"
waittime = 600


ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

while True:
    f = open(logfile,'a')
    ssh.connect(server_IP,port,username, password)
    stdin, stdout, stderr = ssh.exec_command(command)
    output = stdout.read().splitlines()

    for line in output:
        f.write(line.decode("utf-8") + "\n")
    f.close()
    time.sleep(waittime)