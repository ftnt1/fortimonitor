# README #

FMG/FAZ process restart tool

## Files ##

### fmgfazprocessreset.py ###

It was written to help customers automatically restart hanging oftpd on FortiAnalyzer
It reads output of "diag sys process list", extracts lines where "service" is found and pulls out their PID's then restarting these PID's

### README.md ###

This file

## Configuration ##

Edit this part to connect to your device and restart process you want:

server_IP = "FMG/FAZ IP ADDRESS"
username = "USERNAME"
password = "PASSWORD"
port = PORT NUMBER
service = "PROCESS NAME"

### Example ###

server_IP = "10.0.0.1"
username = "admin"
password = "password"
port = 22
service = "oftpd"